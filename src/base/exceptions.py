from rest_framework.exceptions import APIException


class APIError(APIException):
    def __init__(self, status_code=None, detail=None):
        super(APIError, self).__init__(detail=detail)
        if status_code is not None:
            self.status_code = status_code

    def __str__(self):
        return self.detail
