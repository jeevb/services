from django.forms.models import model_to_dict
from collections import defaultdict


class TrackedModelMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """

    untracked = None

    def __init__(self, *args, **kwargs):
        super(TrackedModelMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def changes(self):
        d1 = self.__initial
        d2 = self._dict
        return {
            k: (v, d2[k])
            for k, v in d1.iteritems()
            if v != d2[k]
        }

    @property
    def has_changed(self):
        return bool(self.changes)

    @property
    def changed_fields(self):
        return self.changes.keys()

    def get_tracked_fields(self):
        untracked = self.untracked or []
        return [f.name for f in self._meta.fields if not f.name in untracked]

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(TrackedModelMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=self.get_tracked_fields())


class ReadOnlyModelAdminMixin(object):
    def get_readonly_fields(self, request, obj=None):
        return self.model._meta.get_all_field_names()

    def has_add_permission(self, request):
        # Nobody is allowed to add
        return False

    def has_delete_permission(self, request, obj=None):
        # Nobody is allowed to delete
        return False
