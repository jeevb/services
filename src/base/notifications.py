from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import ParseError
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from base.utils import get_client_ip


class GenericNotification(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    renderer_classes = (JSONRenderer,)

    def _handle_request(self, request, data_attr='data'):
        # Extract the required data from the request
        user = request.user
        client_ip = get_client_ip(request)
        data = getattr(request, data_attr)

        serializer_class = self.get_serializer_class()
        serializer = serializer_class(data=data)
        if not serializer.is_valid():
            raise ParseError

        # Handle the request
        task = self.get_task_handler()
        task.delay(user.pk, client_ip, **serializer.validated_data)

        return Response()

    def get_serializer_class(self):
        assert self.serializer_class is not None, (
            "%r should either include a `serializer_class` attribute, "
            "or override the `get_serializer_class()` method."
            % self.__class__.__name__
        )

        return self.serializer_class

    def get_task_handler(self):
        assert self.task_handler is not None, (
            "%r should either include a `task_handler` attribute, "
            "or override the `get_task_handler()` method."
            % self.__class__.__name__
        )

        return self.task_handler

    def get(self, request):
        return self._handle_request(request, 'query_params')

    def post(self, request):
        return self._handle_request(request)
