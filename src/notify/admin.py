from django.contrib import admin
from notify.models import (
    Bot,
    SlackChannel,
    HipChatUser,
    HipChatRoom,
    HipChatRoomNotification
)


@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    list_display = ('name', 'icon',)
    search_fields = ('name',)


@admin.register(SlackChannel)
class SlackChannelAdmin(admin.ModelAdmin):
    list_display = ('name', 'bot', 'channel', 'default',)
    list_filter = ('bot', 'default',)
    search_fields = ('name', 'bot', 'channel',)


@admin.register(HipChatUser)
class HipChatUserAdmin(admin.ModelAdmin):
    list_display = ('name', 'bot', 'user_id', 'default',)
    list_filter = ('bot', 'default',)
    search_fields = ('name', 'bot', 'user_id',)


@admin.register(HipChatRoom, HipChatRoomNotification)
class HipChatRoomAdmin(admin.ModelAdmin):
    list_display = ('name', 'bot', 'room_id', 'default',)
    list_filter = ('bot', 'default',)
    search_fields = ('name', 'bot', 'room_id',)
