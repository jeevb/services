# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-05 00:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Bot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('icon', models.URLField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Route',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('default', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='HipChatRoom',
            fields=[
                ('route_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='notify.Route')),
                ('room_id', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'HipChat rooms',
                'verbose_name': 'HipChat room',
            },
            bases=('notify.route', models.Model),
        ),
        migrations.CreateModel(
            name='HipChatRoomNotification',
            fields=[
                ('route_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='notify.Route')),
                ('room_id', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'HipChat room notifications',
                'verbose_name': 'HipChat room notication',
            },
            bases=('notify.route', models.Model),
        ),
        migrations.CreateModel(
            name='HipChatUser',
            fields=[
                ('route_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='notify.Route')),
                ('user_id', models.CharField(max_length=100)),
            ],
            options={
                'verbose_name_plural': 'HipChat users',
                'verbose_name': 'HipChat user',
            },
            bases=('notify.route',),
        ),
        migrations.CreateModel(
            name='SlackChannel',
            fields=[
                ('route_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='notify.Route')),
                ('channel', models.CharField(max_length=50)),
            ],
            bases=('notify.route',),
        ),
        migrations.AddField(
            model_name='route',
            name='bot',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='routes', to='notify.Bot'),
        ),
    ]
