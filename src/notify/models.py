import urllib

from django.db import models
from model_utils.managers import InheritanceManager
from notify.requests import (
    SlackMessageRequest,
    HipChatPrivateMessageRequest,
    HipChatRoomMessageRequest,
    HipChatRoomNotificationRequest
)
from notify.settings import notify_settings as settings


class Bot(models.Model):
    name = models.CharField(max_length=50)
    icon = models.URLField(blank=True)

    def __str__(self):
        return self.name


class Route(models.Model):
    name = models.CharField(max_length=50)
    bot = models.ForeignKey(Bot, related_name='routes')
    default = models.BooleanField(default=False)

    objects = InheritanceManager()

    def send(self, text):
        raise NotImplementedError(
            '%r should implement the `send()` method.'
            % self.__class__.__name__
        )

    def __str__(self):
        return self.name


class SlackChannel(Route):
    channel = models.CharField(max_length=50)

    def send(self, text):
        request = SlackMessageRequest()
        request(text=text,
                channel=self.channel,
                username=self.bot.name,
                icon_url=self.bot.icon,
                token=settings.SLACK_API_TOKEN)


class HipChatUser(Route):
    user_id = models.CharField(max_length=100)

    class Meta:
        verbose_name = 'HipChat user'
        verbose_name_plural = 'HipChat users'

    def send(self, text):
        request = HipChatPrivateMessageRequest()
        request(url_args={'user_id': self.user_id},
                message=text,
                notify='true',
                message_format='text',
                token=settings.HIPCHAT_API_TOKEN)


class HipChatRoomMixin(models.Model):
    room_id = models.CharField(max_length=100)

    @property
    def url_encoded_room_id(self):
        return urllib.parse.quote(self.room_id)

    class Meta:
        abstract = True


class HipChatRoom(HipChatRoomMixin, Route):
    class Meta:
        verbose_name = 'HipChat room'
        verbose_name_plural = 'HipChat rooms'

    def send(self, text):
        request = HipChatRoomMessageRequest()
        request(url_args={'room_id': self.url_encoded_room_id},
                message=text,
                token=settings.HIPCHAT_API_TOKEN)


class HipChatRoomNotification(HipChatRoomMixin, Route):
    class Meta:
        verbose_name = 'HipChat room notication'
        verbose_name_plural = 'HipChat room notifications'

    def send(self, text):
        request = HipChatRoomNotificationRequest()
        request(url_args={'room_id': self.url_encoded_room_id},
                message=text,
                notify='true',
                message_format='text',
                color='random',
                token=settings.HIPCHAT_API_TOKEN)
