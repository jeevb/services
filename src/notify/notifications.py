from base.notifications import GenericNotification
from notify.serializers import DataSerializer
from notify.tasks import send_notification


class Notification(GenericNotification):
    serializer_class = DataSerializer
    task_handler = send_notification
