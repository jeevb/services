import json
import requests

from base.generics import PostAPIRequest


class SlackRequest(PostAPIRequest):
    base_url = 'https://slack.com/api'


class SlackMessageRequest(SlackRequest):
    endpoint = '/chat.postMessage'

    def __call__(self, *args, **kwargs):
        return self.response(data=kwargs)


class HipChatRequest(PostAPIRequest):
    base_url = 'http://api.hipchat.com/v2'

    def __call__(self, *args, **kwargs):
        url_args = kwargs.pop('url_args')
        token = kwargs.pop('token')
        headers = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }
        return self.response(url_args, headers=headers, json=kwargs)


class HipChatPrivateMessageRequest(HipChatRequest):
    endpoint = '/user/{user_id}/message'
    expected_codes = (requests.codes.ok, requests.codes.NO_CONTENT,)


class HipChatRoomMessageRequest(HipChatRequest):
    endpoint = '/room/{room_id}/message'
    expected_codes = (requests.codes.ok, requests.codes.CREATED,)


class HipChatRoomNotificationRequest(HipChatRequest):
    endpoint = '/room/{room_id}/notification'
    expected_codes = (requests.codes.ok, requests.codes.NO_CONTENT,)
