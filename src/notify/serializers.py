from rest_framework import serializers


class DataSerializer(serializers.Serializer):
    text = serializers.CharField()
    to = serializers.ListField(
        child=serializers.CharField(max_length=50)
    )
