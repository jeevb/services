from django.conf import settings
from django.dispatch import receiver
from django.test.signals import setting_changed
from base.settings import BaseAppSettings

USER_SETTINGS = getattr(settings, 'NOTIFY_SETTINGS', None)

DEFAULTS = {
    # Slack settings
    'SLACK_API_TOKEN': '',

    # HipChat settings
    'HIPCHAT_API_TOKEN': '',
}


class NotifySettings(BaseAppSettings):
    pass


notify_settings = NotifySettings(USER_SETTINGS, DEFAULTS)


@receiver(setting_changed)
def reload_notify_settings(*args, **kwargs):
    global notify_settings
    setting, value = kwargs['setting'], kwargs['value']
    if setting == 'NOTIFY_SETTINGS':
        notify_settings = NotifySettings(value, DEFAULTS)
