import logging

from celery import shared_task
from base.exceptions import APIError
from notify.models import Route

logger = logging.getLogger(__name__)

@shared_task
def send_notification(user_id, client_ip, **kwargs):
    # Use default routes if no routes are specified
    to = kwargs.get('to')
    routes = (
        Route.objects.filter(name__in=to) if to
        else Route.objects.filter(default=True)
    )

    text = kwargs.get('text')
    for route in routes.select_subclasses():
        try:
            route.send(text)
        except APIError as e:
            logger.error(
                'Message forwarding failed for <User ID: {}> '
                'from <IP: {}> to <Route: {}::{}> with '
                '<Error: {} {}>: "{}".'.format(
                    user_id,
                    client_ip,
                    route.__class__.__name__,
                    route.name,
                    e.status_code,
                    e.detail,
                    text
                )
            )
        except Exception as e:
            logger.exception(e)
