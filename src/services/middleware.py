import re

from django.conf import settings
from django.http import HttpResponseForbidden
from ipaddress import ip_address, ip_network


class ProtectedPathMiddleware(object):
    def process_request(self, request):
        for path in settings.IP_PROTECTED_PATHS:
            if re.search(path, request.path):
                break
        else:
            return

        # Determine requesting IP.
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
        remote_addr = ip_address(
            x_forwarded_for.split(',')[0].strip()
            if x_forwarded_for else
            request.META.get('REMOTE_ADDR', None)
        )

        # Check if network is whitelisted
        for network in settings.WHITELISTED_NETWORKS:
            if remote_addr in ip_network(network):
                return

        return HttpResponseForbidden()
