from django.conf.urls import include, url
from django.contrib import admin
from notify.notifications import Notification

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^notify/', Notification.as_view(), name='notify'),
]
