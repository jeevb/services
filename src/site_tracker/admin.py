from django.contrib import admin
from site_tracker.models import TrackedSite


@admin.register(TrackedSite)
class TrackedSiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'url', 'get_routes')
    search_fields = ('name', 'url',)
