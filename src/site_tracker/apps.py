from django.apps import AppConfig


class SiteTrackerConfig(AppConfig):
    name = 'site_tracker'
    verbose_name = 'Site Tracker'
