import requests

from django.db import models
from notify.models import Route


class TrackedSite(models.Model):
    name = models.CharField(max_length=255)
    url = models.URLField()
    routes = models.ManyToManyField(Route, blank=True)

    def get_routes(self):
        return ', '.join(self.route_names)
    get_routes.short_description = 'Routes'

    @property
    def down_message(self):
        return '{name} ({url}) is down!'.format(name=self.name.capitalize(),
                                                url=self.url)

    @property
    def route_names(self):
        return [r.name for r in self.routes.all()]

    @property
    def is_up(self):
        try:
            response = requests.head(self.url,
                                     allow_redirects=True,
                                     verify=False,
                                     timeout=5)
        except requests.ConnectionError:
            return False
        else:
            return response.status_code == requests.codes.ok

    def __str__(self):
        return self.url
