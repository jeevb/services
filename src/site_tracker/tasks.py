import logging

from celery import shared_task
from site_tracker.models import TrackedSite

logger = logging.getLogger(__name__)

@shared_task
def heartbeat():
    logger.info('Started tracking sites...')
    for site in TrackedSite.objects.all():
        if not site.is_up:
            logger.warning(site.down_message)
            for route in site.routes.select_subclasses():
                route.send(site.down_message)
    logger.info('Site tracking complete.')
